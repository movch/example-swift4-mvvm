//
//  NetworkDataPresentable.swift
//  Swift4MVVMDemo
//
//  Created by Michail Ovchinnikov on 12/10/2017.
//  Copyright © 2017 Michail Ovhcinnikov. All rights reserved.
//

import UIKit

public protocol NetworkDataPresentable: NSObjectProtocol {
    
    associatedtype T: NetworkViewModellable
    var viewModel: T { get set }
    func setupDefaultBindings()
    func bindViewModel()

}

extension NetworkDataPresentable where Self: UIViewController {
    
    func showActivityIndicator() {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        NetworkActivityIndicator.shared.show(view)
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        NetworkActivityIndicator.shared.hide()
    }
    
    func setupDefaultBindings() {
        viewModel.isLoading.bind { [weak self] isLoading -> () in
            if isLoading {
                self?.showActivityIndicator()
            } else {
                self?.hideActivityIndicator()
            }
        }
        
        viewModel.error.bind { [weak self] error -> () in
            if error != nil {
                let alert = UIAlertController(
                    title: "Error", message: error?.localizedDescription,
                    preferredStyle: UIAlertControllerStyle.alert
                )
                
                alert.addAction(UIAlertAction(
                    title: "Ok",
                    style: UIAlertActionStyle.cancel,
                    handler: nil)
                )
                
                self?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
}
