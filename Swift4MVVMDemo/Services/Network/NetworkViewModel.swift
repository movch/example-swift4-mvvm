//
//  NetworkViewModel.swift
//  Swift4MVVMDemo
//
//  Created by Michail Ovchinnikov on 12/10/2017.
//  Copyright © 2017 Michail Ovhcinnikov. All rights reserved.
//

import Foundation

public protocol NetworkViewModellable {

    var service: NetworkService { get set }
    var isLoading: Dynamic<Bool> { get set }
    var error: Dynamic<Error?> { get set }

}

public class NetworkViewModel: NetworkViewModellable {
    
    public var service: NetworkService
    public var isLoading: Dynamic<Bool> = Dynamic(false)
    public var error: Dynamic<Error?> = Dynamic(nil)
    
    init(service: NetworkService) {
        self.service = service
    }
    
    public func perform<T: Codable>(request: URLRequest, completion: @escaping (T) -> ()) {
        isLoading.value = true
        
        service.perform(request: request) { [weak self] (result: T?, error: Error?) -> () in
            self?.isLoading.value = false
            
            if error == nil, let receivedData = result {
               completion(receivedData)
            } else {
                self?.error.value = error
            }
        }
    }
    
}
