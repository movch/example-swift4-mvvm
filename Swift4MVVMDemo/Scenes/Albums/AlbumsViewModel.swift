//
//  AlbumsViewModel.swift
//  Swift4MVVMDemo
//
//  Created by Michail Ovchinnikov on 11/10/2017.
//  Copyright © 2017 Michail Ovhcinnikov. All rights reserved.
//

import Foundation

final class AlbumsViewModel: NetworkViewModel {
    
    var albums: Dynamic<[Album]> = Dynamic([])
    
    func getAlbums() {
        guard let url = URL(string: "\(NetworkSettings.serverURL)/albums") else { return }
        let request = URLRequest(url: url)
        
        perform(request: request) { [weak self] (result: [Album]) -> () in
            DispatchQueue.main.async {
                self?.albums.value = result
            }
        }
    }
    
}
