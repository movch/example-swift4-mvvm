//
//  TodayTableViewController.swift
//  Swift4MVPDemo
//
//  Created by Michael Ovchinnikov on 23/09/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

import UIKit

final class AlbumsViewController: UITableViewController {
    
    var viewModel = AlbumsViewModel(service: NetworkService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Albums"
        
        // Don't forget to call bindings setup for view model
        setupDefaultBindings()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.getAlbums()
    }
    
}

// MARK: - NetworkDataPresentable

extension AlbumsViewController: NetworkDataPresentable {
    
    func bindViewModel() {
        viewModel.albums.bind { [weak self] _ -> () in
            self?.tableView.reloadData()
        }
    }
    
}

// MARK: - UITableViewController

extension AlbumsViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.albums.value.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "albumCell", for: indexPath)
        cell.textLabel?.text = viewModel.albums.value[indexPath.row].title
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let factory = ViewControllerFactory(storyboard: UIStoryboard(name: "Main", bundle: nil))
        let router = Router(
            navigationController: navigationController,
            viewControllerFactory: factory
        )
        let album = viewModel.albums.value[indexPath.row]
        router.presentPhotos(forAlbum: album)
    }
}
