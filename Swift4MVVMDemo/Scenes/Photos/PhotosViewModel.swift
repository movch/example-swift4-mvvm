//
//  PhotosViewModel.swift
//  Swift4MVVMDemo
//
//  Created by Michail Ovchinnikov on 12/10/2017.
//  Copyright © 2017 Michail Ovhcinnikov. All rights reserved.
//

import Foundation

final class PhotosViewModel: NetworkViewModel {
    
    var photos: Dynamic<[Photo]> = Dynamic([])
    
    func getPhotos(forAlbum albumID: Int) {
        guard let url = URL(string: "\(NetworkSettings.serverURL)/photos?albumId=\(albumID)") else { return }
        let request = URLRequest(url: url)
        
        perform(request: request) { [weak self] (result: [Photo]) -> () in
            DispatchQueue.main.async {
                self?.photos.value = result
            }
        }
    }
    
}
