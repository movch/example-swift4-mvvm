//
//  PhotosViewController.swift
//  Swift4MVPDemo
//
//  Created by Michael Ovchinnikov on 24/09/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

import UIKit

final class PhotosViewController: UITableViewController {
    
    var album: Album?
    var viewModel = PhotosViewModel(service: NetworkService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Photos"
        
        setupDefaultBindings()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let album = self.album else { return }
        title = album.title
        
        guard let albumID = album.id else { return }
        viewModel.getPhotos(forAlbum: albumID)
    }
    
}

// MARK: - NetworkDataPresentable

extension PhotosViewController: NetworkDataPresentable {
    
    func bindViewModel() {
        viewModel.photos.bind { [weak self] _ -> () in
            self?.tableView.reloadData()
        }
    }
    
}

// MARK: - Table view data source

extension PhotosViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.photos.value.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "photoCell", for: indexPath)
        cell.textLabel?.text = viewModel.photos.value[indexPath.row].title
        return cell
    }
    
}
